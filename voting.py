#!/usr/bin/env python
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Processes Ted Talk winners from google form data')
parser.add_argument('file_to_analyze',
                    help='the google forms data to analyze (csv format)')
parser.add_argument('--output', dest='output_file',
                    help='the name of the output file to generate (pdf)')

args = parser.parse_args()

# the file to analyze
file_to_analyze = args.file_to_analyze

# create the responses dataframe and reset the index
responses_dataframe = pd.read_csv(file_to_analyze).transpose()
responses_dataframe.reset_index(inplace=True)

# create the outfit and presentation dataframes by filtering on the 'index' column
outfit_dataframe = responses_dataframe.loc[
    responses_dataframe['index'].str.contains('Outfit', regex=False)==True]
presentation_dataframe = responses_dataframe.loc[
    responses_dataframe['index'].str.contains('Presentation', regex=False)==True]

def calculate_averages(df: pd.DataFrame) -> pd.DataFrame:
    """Given a pandas dataframe df, average out votes for each presenter"""

    column_names = [thing for thing in responses_dataframe.columns if thing != 'index']
    df_means = df.filter(items=column_names).mean(axis=1)
    mean_list = []
    title_list = []
    for i in range(len(df)):
        mean_list.append(df_means.iloc[i])
        title_list.append(df.iloc[i]['index'])
        
    return pd.DataFrame({'means': mean_list, 'titles': title_list})

presentation_results = calculate_averages(presentation_dataframe) \
    .sort_values('means', ascending=False)

outfit_results = calculate_averages(outfit_dataframe) \
    .sort_values('means', ascending=False)

def create_winners_dict(results:pd.DataFrame) -> dict:
    winners = {}
    for i in range(3):
        score = results[i:i+1]['means'].iloc[0]
        (name, title) = results[i:i+1]['titles'] \
                            .iloc[0].split(' -- ')
        title = title.split(' [')[0]

        winners[i+1] = {'name': name,
                        'title': title,
                        'score': score}

    return winners

pres_winners = create_winners_dict(presentation_results)
outfit_winners = create_winners_dict(outfit_results)

if args.output_file:
    f = open(args.output_file, 'w')
    print(f'writing output to {args.output_file}')
    # https://stackoverflow.com/questions/33155776/export-pandas-dataframe-into-a-pdf-file-using-python

    f.write("# Ted Talk Winners\n\n")

    f.write(f"### Best Presentation: {pres_winners[1]['name']}\n\n")
    f.write(f"{pres_winners[1]['title']} (Score={pres_winners[1]['score']:.2f})\n\n")

    f.write(f"### Best Dressed: {outfit_winners[1]['name']}\n\n")
    f.write(f"{outfit_winners[1]['title']} (Score={outfit_winners[1]['score']:.2f})\n\n")

    f.write(f"### Best Timed: TBD\n\n")
    f.write(f"TBD (Score = TBD)\n\n")

    f.write('---\n\n')

    f.write("#### Runners Up\n\n")

    f.write("##### Best Presentation\n\n")
    for idx in range(2,4):
        name = pres_winners[idx]['name']
        title = pres_winners[idx]['title']
        score = pres_winners[idx]['score']
        f.write(f"{name} -- {title} (Score={score:.2f})\n\n")

    f.write("##### Best Dressed\n\n")
    for idx in range(2,4):
        name = outfit_winners[idx]['name']
        title = outfit_winners[idx]['title']
        score = outfit_winners[idx]['score']
        f.write(f"{name} -- {title} (Score={score:.2f})\n\n")

    f.write("##### Best Timed\n\n")
    for _ in range(2):
        f.write(f"TBD -- TBD (Score=TBD)\n\n")

    f.close()
else:

    print("\n----------Winners----------\n\n")

    print(f"Best Presentation: {pres_winners[1]['name']}\n")
    print(f"    {pres_winners[1]['title']} (Score={pres_winners[1]['score']:.2f})\n\n")

    print(f"Best Dressed: {outfit_winners[1]['name']}\n")
    print(f"    {outfit_winners[1]['title']} (Score={outfit_winners[1]['score']:.2f})\n\n")

