# Ted Talk: Pandemic Winners

### Best Presentation: Kim Whitney
DDZDD: Dads, Daddies, Zaddies & Dom Daddies (Score=7.0)

### Best Dressed: Sofía Benítez
Shakira. Just. Shakira. (Score=x.x)

### Best Timed: Connor Rouan
Examining Himalayan Expressionism in a Post-Modern Society (Time=2:59.5)

---

#### Runners Up

##### Best Presentation
Person One -- Presentation Title (score=x.x)

Person Two -- Presentation Title (score=x.x)

#### Best Dressed
Person One -- Presentation Title (score=x.x)

Person Two -- Presentation Title (score=x.x)

#### Best Timed
Person One -- Presentation Title (time=x:x.x)

Person Two -- Presentation Title (time=x:x.x)
