## Initial Setup

`python3 -m venv env`

`source env/bin/activate`

## Requirements

See `requirements.txt` for python requirements

Also, `pdflatex`

## Usage

### Generate an agenda

Modify `create_agenda_helper.py` for your specific google forms `*.csv`, title, and header image. Then, run it with python `python create_agenda_helper.py`. This will generate a pdf `updated_agenda.pdf` with the agenda, and `names_and_presentations.txt` with a list of presenters and their titles for pasing into the voting google form.

### Tally Votes

Run `python voting.py sample_votes.csv` but replace `sample_votes.csv` with your voting data from the google form. Optinally add a `--output_file output_file.md` to create a formatted markdown file with the winners and runners up (instead of printing to the console).