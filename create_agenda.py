#!/usr/bin/env python
import argparse
import subprocess
import pandas as pd

def parse_args():
  # add arguments to parser
  parser = argparse.ArgumentParser(description='Generate Ted Talk agenda from google form results')
  parser.add_argument('file_to_analyze',
                      help='the google forms data to parse (csv format)')
  parser.add_argument('--date', type=str, required=True,
                      help='The date of the event')
  parser.add_argument('--voting_url', type=str, required=True,
                      help='The URL to submit votes to')
  parser.add_argument('--time', type=str, required=True,
                      help='Start time for the presentations')
  parser.add_argument('--subheading', type=str, required=True,
                      help='Agenda subheading')
  args = parser.parse_args()


def create_agenda(file_to_analyze: str, voting_url: str, subheading: str, date: str, time: str):
  # create the responses dataframe and reset the index
  responses = pd.read_csv(file_to_analyze)

  # randomize order
  responses = responses.sample(frac=1).reset_index(drop=True)

  titles = responses["Presentation Title"]
  names = responses["Full Name (as will appear in the agenda)"]

  f = open('name_and_presentation.txt', 'w')

  # latex table formatting and writing names -- titles to file
  names_and_titles = ''
  for index in range(len(responses)):
      f.write(f'{names[index]} -- {titles[index]}\n')

      # escape latex special characters
      name = names[index].replace('&', '\\&')
      title = titles[index].replace('&', '\\&')

      row = f' {names[index]} & {titles[index]} & & \\\ \n \cline{{3-4}}\n'
      names_and_titles += row

  f.close()

  # TODO: bug - if no one title stretches across two lines, the table formatting is off

  # Read in the latex template
  with open('agenda_template.tex', 'r') as file :
    filedata = file.read()

  # Replace the target strings
  filedata = filedata.replace('NAMES-AND-TITLES', names_and_titles)
  filedata = filedata.replace('VOTE-URL', voting_url)
  filedata = filedata.replace('SUB-TITLE', subheading)
  filedata = filedata.replace('EVENT-DATE', date)
  filedata = filedata.replace('START-TIME', time)

  # Write the file out again
  with open('updated_agenda.tex', 'w') as file:
    file.write(filedata)

  # TODO: figure out how to rename the output file
  subprocess.run(["pdflatex", "updated_agenda.tex"])

  # TODO: generate question template for voting form


if __name__ == "__main__":
    create_agenda(
      file_to_analyze="sample_titles.csv", 
      voting_url="https://www.google.com/",
      subheading="SAMPLE EDITION",
      date="29 Mar 2020",
      time="5:30 pacific"
    )
